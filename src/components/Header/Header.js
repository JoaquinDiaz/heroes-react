import React, { Component } from 'react';
import './Header.css';


class Header extends Component {
  render() {
    return (
        <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand">
                Brastlewark Heroes!
            </a>
        </nav>
    );
  }
}

export default Header;
