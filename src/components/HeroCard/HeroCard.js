import React, {Component} from 'react';
import { Link } from 'react-router-dom'
import './HeroCard.css';

class HeroCard extends Component {
    constructor(hero) {
        super()
        this.state = hero;
    }
    render() {
        return (
            <div>
                <br/>
                <div className="col-sm-4">
                    <div className="card card-home">
                        <img className="card-img-top"
                             src={this.state.hero.thumbnail}
                             alt={this.state.hero.name}
                             />
                        <div className="card-body">
                            <h4 className="card-title"> {this.state.hero.name}</h4>
                            <Link className="btn btn-outline btn-primary"
                                  to={`/auction-detail/${this.state.hero.id}`}>
                                Details
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HeroCard;
