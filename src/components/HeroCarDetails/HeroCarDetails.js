import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import BrastlewarkAPI from '../../api'
import './HeroCarDetails.css';

class HeroCarDetails extends Component {

    constructor(){
        super()
        this.state = {
            hero : null
        }
    }

    componentWillMount(){
        BrastlewarkAPI.getById(
            parseInt(this.props.match.params.number,10)
        ).then((hero)=>{
            this.setState({hero:hero})
        })
    }

    render() {

        if (!this.state.hero) return <div>Sorry, but the hero does't found</div>
        
        return (
            <div className="container">
                <br/>
                <div className="card mb-3 bg-light">
                    <img src={this.state.hero.thumbnail}
                         alt={this.state.hero.name}/>
                    <div className="card-body">
                        <p>Id : <strong>{this.state.hero.id}</strong></p>
                        <h4 className="card-title">Name: {this.state.hero.name}</h4>
                        <p className="card-text">Age: {this.state.hero.age}</p>
                        <p className="card-text">Weight: {this.state.hero.weight}</p>
                        <Link className="btn btn-outline btn-primary"
                              to={`/`}>
                            Back
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default HeroCarDetails;
