import React from 'react'
import {Switch, Route} from 'react-router-dom'
import Home from '../Home/Home'
import HeroCarDetails from '../HeroCarDetails/HeroCarDetails'

const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/auction-detail/:number' component={HeroCarDetails}/>
        </Switch>
    </main>
)

export default Main