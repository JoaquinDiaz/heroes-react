import React, {Component} from 'react';
import BrastlewarkAPI from '../../api'
import HeroCard from '../HeroCard/HeroCard'
import '../Header/Header'
import './Home.css';

class Home extends Component {

    constructor(){
        super()
        this.state = {
            hero:[],
            searchText:'',
        }

        this.interval = null

        this.searchHeroes = this.searchHeroes.bind(this)
    }
    componentWillMount(){

        BrastlewarkAPI.all().then((json)=>{
            this.setState({hero:json})
        })
    }
    searchHeroes(evt){
        const search = evt.target.value
        this.setState({searchText:search})
        this.setState({hero:[]})

        clearTimeout(this.interval);
        this.interval =  setTimeout(()=>{
            BrastlewarkAPI.getByName(search).then((json)=>{
                this.setState({hero:json})
            })
        },800)

    }
    render() {
        const hero = this.state.hero && this.state.hero.Brastlewark &&
            this.state.hero.Brastlewark.map((hero,index)=> {
            return <HeroCard  key={ index } hero={hero}/>;
        })
        return (
            <div className="container">
                <br/><br/>
                <form>
                    <div className="form-label-group">
                        <input type="text"
                               value={this.state.searchText}
                               className="form-control"
                               placeholder="Search your hero!"
                               onChange={evt => this.searchHeroes(evt)}
                        />
                    </div>
                </form>
                <div className="card-group">
                    {hero}
                </div>
            </div>
        );
    }
}

export default Home;
