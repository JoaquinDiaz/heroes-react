/**
 * url to fetch data
 * @type {string}
 */
const dataUrl = 'https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json'

/**
 * We will hold fetched data to array (to avoid unnecessary  requesting)
 * @type {Array}
 */
let heroes = []
/**
 * async wrap , url Api
 * @type {{_fetchUrlData: (function(): Promise), all: (function(): Promise), get: (function(*): Promise)}}
 */
const BrastlewarkAPI = {
    /**
     * internal async singelton
     * @returns {Promise.<*>}
     * @private
     */
    async _fetchUrlData() {

        if(!heroes.length){
            async function processFetch() {
                const response = await fetch(dataUrl)
                return await response.json()
            }

            return processFetch().then((json)=>{
                console.log('fetching data from api')
                heroes.push(json)
                return heroes[0]
            })
        }  console.log('working with in memory')
        return heroes[0]

    },
    /**
     * Api getter
     * @returns {Promise.<*>}
     */
    async all() {
        return await this._fetchUrlData()
    },
    /**
     * Api getter
     * @param id
     * @returns {Promise.<*>}
     */
    async getById(id){
        const json = await this.all()
        const heroId = p => p.id === id
        return await json.Brastlewark.find(heroId)
    },
    /**
     *
     * @param name
     * @returns {Promise.<*>}
     */
    async getByName(name){

        const json = await this.all()
        const filterHero = hero => {
            const n = hero.name.toLowerCase()
            return n.match(name.toLowerCase())!==null
        }
        return {Brastlewark: json.Brastlewark.filter(filterHero)}
    },
}

export default BrastlewarkAPI